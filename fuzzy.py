from typing import Any, Union
import numpy as np
from collections.abc import Sequence
import skfuzzy as fz

def raise_immutable(*args, **kws):
        raise TypeError('object is immutable')

class imdict(dict):
    "immutable dict"
    def __hash__(self):
        return id(self)

    __setitem__ = raise_immutable
    __delitem__ = raise_immutable
    clear       = raise_immutable
    update      = raise_immutable
    setdefault  = raise_immutable
    pop         = raise_immutable
    popitem     = raise_immutable

class fuz:
    "countable fuzzy set base class"
    _data: np.ndarray
    _map: imdict

    __setitem__ = raise_immutable
    __delitem__ = raise_immutable

    @staticmethod
    def create(*args, **kwargs):
        "try to create fuz from arbitrary parameters, return None if fail"
        try: return fuz(*args, **kwargs)
        except: return None

    def __init__(self, obj: Union['fuz',set, dict,Sequence,np.ndarray], mfx: Union[Sequence, np.ndarray] = None):
        """
        params
        ---
        obj: fuz | dict | set | Sequence | np.ndarray
        mfx (optional): Sequence | np.ndarray

        parsing logic
        ---
        - if obj is an instance of fuz, its data is be copied to the new instance
        - if obj is a valid countable finite mapping of x to mf values, a new instance is created ignoring mfx parameter
            - flat dictionaries with numeric values or sequences/ndarrays with shape (2, N) are parsed as x and mfx
            - sets are parsed as x with implicit mfx = 1
        - if obj is 1d sequence/ndarray, mfx is taken from second argument
        """
        assert isinstance(obj, (fuz, set, dict, Sequence, np.ndarray)), "invalid type of data"
        assert isinstance(mfx, (Sequence, np.ndarray, type(None))), "invalid type of mfx"

        if isinstance(obj, fuz):
            assert mfx is None, "ambiguous fuz parameters, given obj requires mfx parameter to be None"
            self._data = np.copy(obj._data)
            self._map = imdict(zip(*self._data))
        elif isinstance(obj, dict):
            assert mfx is None, "ambiguous fuz parameters, given obj requires mfx parameter to be None"
            self._data = np.array(list(obj.items())).T
            self._map = imdict(obj)
        elif isinstance(obj, set):
            assert mfx is None, "ambiguous fuz parameters, given obj requires mfx parameter to be None"
            self._data = np.array([list(obj), np.ones(len(obj))])
            self._map = imdict(zip(*self._data))
        elif isinstance(obj, (Sequence, np.ndarray)):
            obj = np.array(obj)
            x = None
            if obj.ndim == 2 and obj.shape[0] == 2:
                assert mfx is None, "ambiguous fuz parameters, given obj requires mfx parameter to be None"
                x, mfx = obj
            elif obj.ndim == 1:
                assert mfx is not None, "for a given x argument mfx parameter is required"
                x, mfx = obj, np.array(mfx)
                assert 1 ==  mfx.ndim and len(mfx) == len(x), "given mfx is incompactible with x"
            else:
                raise ValueError("invalid fuz parameters")
            self._data = np.array([x, mfx])
            self._map = imdict(zip(x, mfx))

        assert np.issubdtype(self.mf.dtype, np.number), "mfx is not numeric"
        assert len(self.x) == len(self.mf), "incompactible x and mfx"
        if len(self.x) > 0:
            assert self._data[1].min() >= 0 and self._data[1].max() <= 1, "mfx values are not within [0,1] range"

        self._data.flags.writeable = False

    def __iter__(self):
        return iter(self._data)

    def __getitem__(self, key: Any):
        assert type(key) in (int, slice), "invalid key type"
        return self._data[key]

    def __repr__(self):
        if len(self.x) == 0:
            return 'Fuz([])'
        return self._data.__repr__().replace('array', 'Fuz')
    def __str__(self):
        return self._data.__str__().replace('array', 'Fuz')

    def latex(self, struct = 'bmatrix') -> str:
        "return latex representation of fuz"
        assert struct in ('bmatrix', 'tabular'), "struct option invalid, must be 'bmatrix' or 'tabular'"
        return \
        f"""
        \\begin{struct}
            { ' & '.join([str(k) for k in self.x ]) } \\\\
            { ' & '.join([str(k) for k in self.mf ]) }
        \\end{struct}
        """

    def round(self, decimals: int) -> 'fuz':
        "create new fuz with rounded mf"
        return fuz(self.x, np.round(self.mf, decimals))

    @property
    def data(self) -> np.ndarray:
        return self._data
    @property
    def x(self) -> np.ndarray:
        return self._data[0]
    @property
    def mf(self) -> np.ndarray:
        return self._data[1]
    @property
    def map(self) -> dict:
        return self._map

    #HELPERS

    def _fuzzify_obj(self, obj):
        """
        try to convert obj to fuz

        if 1-d data is given and it fits current set as member function,
        the instance x will be used for new fuz

        return fuz object on success, None on failure
        """
        if isinstance(obj, fuz):
            return obj
        elif isinstance(obj, (Sequence, np.ndarray)) and (len(self.x) == len(obj)):
            return fuz(self.x, obj)
        else:
            return fuz.create(obj)

    # SET OPERATIONS

    def __eq__(self, other: 'fuz'):
        if isinstance(other, fuz):
            return np.array_equal(self.data, other.data)
        return False

    def subset_of(self, other: 'fuz'):
        other = self._fuzzify_obj(other)
        if other is None: raise NotImplementedError
        x, mfx, y, mfy = self.x, self.mf, other.x, other.mf

        # this implementation is based on skfuzzy
        sameuniverse = False
        if x.shape == y.shape:
            if (x == y).all():
                mfx2 = mfx
                mfy2 = mfy
                sameuniverse = True

        if not sameuniverse:
            _, mfx2, mfy2 = fz._resampleuniverse(x, mfx, y, mfy)

        return (mfx2 <= mfy2).all()

    def superset_of(self, other: 'fuz'):
        other = self._fuzzify_obj(other)
        if other is None: raise NotImplementedError
        return other.subset_of(self)

    def inv(self) -> 'fuz':
        "set inversion"
        return fuz(self.x, 1. - self.mf)

    def and_(self, other) -> 'fuz':
        "set intersection"
        other = self._fuzzify_obj(other)
        if other is None: raise NotImplementedError
        z, mfz = fz.fuzzy_and(self.x, self.mf, other.x, other.mf)
        return fuz(z, mfz)

    def nand(self, other) -> 'fuz':
        "set intersection inversion"
        return self.and_(other).inv()

    def or_(self, other) -> 'fuz':
        "set union"
        other = self._fuzzify_obj(other)
        if other is None: raise NotImplementedError
        z, mfz = fz.fuzzy_or(self.x, self.mf, other.x, other.mf)
        return fuz(z, mfz)

    def nor(self, other) -> 'fuz':
        "set union inversion"
        return self.or_(other).inv()

    def xor(self, other) -> 'fuz':
        "exclusive or"
        other = self._fuzzify_obj(other)
        if other is None: raise NotImplementedError
        x, mfx, y, mfy = self.x, self.mf, other.x, other.mf

        # this implementation is based on skfuzzy and/or functions
        # with only mfz calculation modified for xor
        sameuniverse = False
        if x.shape == y.shape:
            if (x == y).all():
                z = x
                mfx2 = mfx
                mfy2 = mfy
                sameuniverse = True

        if not sameuniverse:
            z, mfx2, mfy2 = fz._resampleuniverse(x, mfx, y, mfy)

        # xor = (a & ~b) | (~a & b)
        mfz = np.fmax(np.fmin(mfx2, 1. - mfy2), np.fmin(1. - mfx2, mfy2))
        return fuz(z, mfz)

    def eqv(self, other):
        "set equivalence (xor inversion)"
        return self.xor(other).inv()

    def zaychenko_dif(self, other):
        "zaychenko fuzzy set difference"
        other = self._fuzzify_obj(other)
        if other is None: raise NotImplementedError
        x, mfx, y, mfy = self.x, self.mf, other.x, other.mf

        # this implementation is based on skfuzzy and/or functions
        # with only mfz calculation modified for diff
        sameuniverse = False
        if x.shape == y.shape:
            if (x == y).all():
                z = x
                mfx2 = mfx
                mfy2 = mfy
                sameuniverse = True

        if not sameuniverse:
            z, mfx2, mfy2 = fz._resampleuniverse(x, mfx, y, mfy)

        mfz = np.where(mfx2 > mfy2, mfx2 - mfy2, 0)
        return fuz(z, mfz)

    def classic_dif(self, other):
        "set difference"
        other = self._fuzzify_obj(other)
        if other is None: raise NotImplementedError
        return self.and_(other.inv())

    def classic_rdif(self, other):
        "set right difference"
        other = self._fuzzify_obj(other)
        if other is None: raise NotImplementedError
        return other.and_(self.inv())

    def alpha_cut(self, alpha: float) -> 'fuz':
        """
        subset with mf >= given alpha
        values below alpha are set to 0
        """
        assert isinstance(alpha, (float, int)) or np.issubdtype(alpha, np.number), "non-numeric alpha parameter"
        z, mfz = np.copy(self.data)
        below = np.where(self.mf < alpha)
        mfz[below] = 0.
        return fuz(z, mfz)

    def nonzero(self) -> 'fuz':
        "subset with nonzero elements"
        cut = np.where(self.mf > 0)
        z, mfz = self.x[cut], self.mf[cut]
        return fuz(z, mfz)
    
    # ALGEBRAIC OPERATIONS

    def sum(self, other) -> 'fuz':
        "elementwise sum"
        # is not equivalent to fz.fuzzy_add

        other = self._fuzzify_obj(other)
        if other is None: raise NotImplementedError
        x, mfx, y, mfy = self.x, self.mf, other.x, other.mf

        # this implementation is based on skfuzzy and/or functions
        # with only mfz calculation modified for sum
        sameuniverse = False
        if x.shape == y.shape:
            if (x == y).all():
                z = x
                mfx2 = mfx
                mfy2 = mfy
                sameuniverse = True

        if not sameuniverse:
            z, mfx2, mfy2 = fz._resampleuniverse(x, mfx, y, mfy)

        mfz = mfx2 + mfy2
        mfz = np.where(mfz > 1, 1, mfz)
        return fuz(z, mfz)

    def scale(self, l: Union[float, int, np.number]) -> 'fuz':
        "scale mf by factor of l"
        assert isinstance(l, (float, int)) or np.issubdtype(l, np.number), "non-numeric argument"
        mfz = self.mf * l
        mfz = np.where(mfz > 1, 1, mfz)
        return fuz(self.x, mfz)

    # INTERFACE EXTENSION AND DERIVED FUNCTIONS
    def invert(self):
        return self.inv()
    def __inv__(self):
        return self.inv()
    def __invert__(self):
        return self.inv()

    def __and__(self, other):
        return self.and_(other)
    def __rand__(self, other):
        return self.and_(other)
    def nand(self, other):
        return self.and_(other).inv()

    def __or__(self, other):
        return self.or_(other)
    def __ror__(self, other):
        return self.or_(other)
    def nor(self, other):
        return self.or_(other).inv()

    def __xor__(self, other):
        return self.xor(other)
    def __rxor__(self, other):
        return self.xor(other)

    def difference(self, other):
        return self.zaychenko_dif(other)
    def floordiv(self, other):
        return self.zaychenko_dif(other)
    def __floordiv__(self, other):
        return self.zaychenko_dif(other)

    def __rfloordiv__(self, other):
        return self.rdif(other)

    def __add__(self, other):
        return self.sum(other)
    # def __sub__(self, other):
        # return self.sum(~other)

    def __mul__(self, other):
        return self.scale(other)

    # def truediv(self, other):
        # return self.scale(1/other)
    # def __truediv__(self, other):
        # return self.scale(1/other)

    # def __rtruediv__(self, other):
        # return self.rdif(other)

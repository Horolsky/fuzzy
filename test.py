from fuzzy import fuz
import numpy as np
import pandas as pd

if __name__ == "__main__":

    # TASK CONDITIONS
    x = np.arange(1,11)

    mfxa = np.array([ 0.4, 0.3, 0.8, 0.8, 0. , 0.7, 0.3, 0.4, 0.3, 0.3 ]) 
    mfxb = np.array([ 0.6, 0.9, 0.2, 0.4, 0.9, 0. , 0.1, 0.2, 0.7, 0.5 ]) 
    mfxc = np.array([ 0.7, 0.2, 0.6, 0.3, 0.9, 0.6, 0.4, 0.4, 0.9, 0.3 ]) 

    weights = 0.7, 0.1, 0.2

    A = fuz(x, mfxa)
    B = fuz(x, mfxb)
    C = fuz(x, mfxc)

    Aw = A * weights[0]
    Bw = B * weights[1]
    Cw = C * weights[2]

    data = {
        #condition
        'x': x,
        '\mu_A(x)': A.mf,
        '\mu_B(x)': B.mf,
        '\mu_C(x)': C.mf,
        # solution
        '\lambda_A \mu_A(x)': Aw.mf,
        '\lambda_B \mu_B(x)': Bw.mf,
        '\lambda_C \mu_C(x)': Cw.mf,
        '\mu_{\lambda}(x)': (Aw + Bw + Cw).mf
    }
    print(A.subset_of(A))
    print(A.subset_of(Aw))
    print(A.superset_of(Aw))
